

const GRAPHQL_URL  = "http://localhost:9000/"

async function fetchGreeting(){

    const respone = await fetch(GRAPHQL_URL, {
        method:"POST",
        headers:{
            'content-type' : 'application/json'
        },
        body:JSON.stringify({
            query:`
            query {
                greeting
              }
            `
        })

    });

    const {data} = await respone.json();
    console.log(data)
    return data;
}


fetchGreeting().then(({greeting})=>{
    const title = document.querySelector('h1');
    title.textContent=greeting;

});