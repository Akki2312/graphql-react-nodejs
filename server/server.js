const {ApolloServer, gql} = require('apollo-server');

//The most basic components of a GraphQL schema are object types,
//which just represent a kind of object you can fetch from your service, and what fields it has. 
//In the GraphQL schema language, we might represent it like this

const typeDefs= gql`
    schema {
        query: Welcome
    }

    type Welcome {
        greeting: String
    }
`;

//typedefinations are the interface of our API and resolvers are implimentations 

const resolvers = {
    Welcome:{
        greeting : () => 'Hello  Graphql world!'
    }
}

const server = new ApolloServer({typeDefs, resolvers});
server.listen({port:9000})
    .then(({url}) => console.log(`Server running at ${url}`));

